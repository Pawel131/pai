import {Component} from "react";
import React from "react";
import {Form, Input, Button, notification, Row, Col} from 'antd';
import {Redirect} from 'react-router'
import 'antd/dist/antd.css';
import {Link} from "react-router-dom";
import '../../styles/auth.css';
import {ACCESS_TOKEN} from "../../common/constants";
import {login} from "../../common/APIUtils";
import jwt_decode from "jwt-decode";

const FormItem = Form.Item;

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}


class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            doctorRedirect: false,
            patientRedirect: false
        };
        this.postData = this.postData.bind(this);
    }

    componentDidMount() {
        this.props.form.validateFields();
    }

    postData(event) {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                login(values).then(data => {
                    localStorage.setItem(ACCESS_TOKEN, data.token);
                    const isDoctor = jwt_decode(data.token).role === "doctor";
                    this.setState({doctorRedirect: isDoctor, patientRedirect: !isDoctor});
                }).catch(error => {
                    notification.error({
                        message: 'Login Error',
                        description: error.message || 'Sorry! Something went wrong. Please try again!',
                    });
                })
            }
        });
    }

    render() {
        const {getFieldDecorator, getFieldsError, getFieldError, isFieldTouched} = this.props.form;
        const userNameError = isFieldTouched('userName') && getFieldError('email');
        const passwordError = isFieldTouched('password') && getFieldError('password');
        const {doctorRedirect, patientRedirect} = this.state;
        return (
            <Row justify={'center'} type={'flex'}>
                <Col sm={8} xs={24}>
                    <FormItem
                        validateStatus={userNameError ? 'error' : ''}
                        help={userNameError || ''}>
                        {
                            getFieldDecorator('email', {
                                rules: [{required: true, message: 'Please input your username!'}],
                            })(
                                <Input
                                    placeholder="Username"/>
                            )
                        }
                    </FormItem>
                    <FormItem
                        validateStatus={passwordError ? 'error' : ''}
                        help={passwordError || ''}>
                        {
                            getFieldDecorator('password', {
                                rules: [{required: true, message: 'Please input your Password!'}],
                            })(
                                <Input
                                    type="password"
                                    placeholder="Password"/>
                            )
                        }
                    </FormItem>
                    <FormItem>
                        <div className={"col-sm-12"}>
                            <Button
                                onClick={this.postData}
                                disabled={hasErrors(getFieldsError())}>
                                Log in
                            </Button>
                        </div>
                        <div className={'col-sm-12'}>
                            <Link to='/register'><Button type="primary">Register</Button></Link>
                        </div>
                    </FormItem>
                    {doctorRedirect && (
                        <Redirect to={'/dashboard'}/>
                    )}
                    {patientRedirect && (
                        <Redirect to={'/patientPanel'}/>
                    )}
                </Col>
            </Row>
        );
    }
}

class Login extends Component {
    render() {
        const AntWrappedLoginForm = Form.create()(LoginForm);
        return (
            <div className="auth-form">
                <h1 className="text-center">Login</h1>
                <div>
                    <AntWrappedLoginForm/>
                </div>
            </div>
        );
    }
}

export default Login;
