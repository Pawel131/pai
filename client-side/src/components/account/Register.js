import {Component} from "react";
import React from "react";
import {Form, Input, Button, notification, Row, Col} from 'antd';
import {Redirect} from 'react-router'
import 'antd/dist/antd.css';
import {ACCESS_TOKEN} from "../../common/constants";
import {register} from "../../common/APIUtils";
import '../../styles/auth.css';

const FormItem = Form.Item;

class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            patientRedirect: false,
            email: {value: ''},
            password: {value: ''},
            passwordConfirm: {value: ''}
        };
        this.postData = this.postData.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        // this.checkIsEmailUnique = this.checkIsEmailUnique.bind(this);
        this.isFormInvalid = this.isFormInvalid.bind(this);
        this.passwordConfirmHandleChange = this.passwordConfirmHandleChange.bind(this);
    }

    handleInputChange(event, validationFun) {
        const target = event.target;
        const inputName = target.name;
        const inputValue = target.value;

        this.setState({
            [inputName]: {
                value: inputValue,
                ...validationFun(inputValue)
            }
        });
    }

    passwordConfirmHandleChange(event, validationFun) {
        const target = event.target;
        const inputName = target.name;
        const inputValue = target.value;

        this.setState({
            [inputName]: {
                value: inputValue,
                ...validationFun(inputValue, this.state.password.value)
            }
        });
    }

    isFormInvalid() {
        return !(this.state.email.validateStatus === 'success' &&
            this.state.password.validateStatus === 'success' &&
            this.state.passwordConfirm.validateStatus === 'success'
        );
    }


    postData(event) {
        event.preventDefault();
        const registerData = {
            email: this.state.email.value,
            password: this.state.password.value
        };
        register(registerData).then(data => {
            localStorage.setItem(ACCESS_TOKEN, data.token);
            this.setState({patientRedirect: true});
        }).catch(error => {
            notification.error({
                message: 'Register Error',
                description: error.message,
            });
        })
    }

    render() {
        const {patientRedirect, email, passwordConfirm, password} = this.state;
        return (
            <Row justify={'center'} type={'flex'}>
                <Col sm={8} xs={24}>
                    <Form onSubmit={this.postData}>
                        <FormItem
                            label="Email"
                            validateStatus={email.validateStatus}
                            colon={false}
                            help={email.errorMsg}>
                            <Input
                                name={"email"}
                                value={email.value}
                                onChange={(event) => this.handleInputChange(event, this.emailValidate)}
                                placeholder="Provide your email"/>
                        </FormItem>
                        <FormItem
                            label="Password"
                            validateStatus={password.validateStatus}
                            colon={false}
                            help={password.errorMsg}>
                            <Input
                                name={"password"}
                                value={password.value}
                                autoComplete="off"
                                type={"password"}
                                onChange={(event) => this.handleInputChange(event, this.passwordValidate)}
                                placeholder="Password and password confirm must be the same"/>
                        </FormItem>
                        <FormItem
                            label="Password Confirm"
                            validateStatus={passwordConfirm.validateStatus}
                            colon={false}
                            help={passwordConfirm.errorMsg}>
                            <Input
                                name={"passwordConfirm"}
                                value={passwordConfirm.value}
                                type={"password"}
                                onChange={(event) => this.passwordConfirmHandleChange(event, this.passwordConfirmValidate)}
                                placeholder="Password and password confirm must be the same"/>
                        </FormItem>
                        <FormItem>
                            <Button
                                htmlType="submit"
                                type={"primary"}
                                disabled={this.isFormInvalid()}>
                                Register
                            </Button>
                        </FormItem>
                        {patientRedirect && (
                            <Redirect to={'/patientPanel'}/>
                        )}
                    </Form>
                </Col>
            </Row>
        );
    }

    emailValidate = (email) => {
        if (!email) {
            return {
                validateStatus: 'error',
                errorMsg: 'Email may not be empty'
            }
        }

        const EMAIL_REGEX = RegExp('[^@ ]+@[^@ ]+\\.[^@ ]+');
        if (!EMAIL_REGEX.test(email)) {
            return {
                validateStatus: 'error',
                errorMsg: 'Email not valid'
            }
        }
        return {
            validateStatus: 'success',
            errorMsg: null
        }
    };

    passwordValidate = (password) => {
        if (password.length < 3 || password.length > 10) {
            return {
                validateStatus: 'error',
                errorMsg: 'Password size must be  between 3 and 10'
            }
        }

        return {
            validateStatus: 'success',
            errorMsg: null
        }
    };

    passwordConfirmValidate = (password, passwordConfirm) => {
        if (password !== passwordConfirm) {
            return {
                validateStatus: 'error',
                errorMsg: 'Password and confirm must be the same'
            }
        }

        return {
            validateStatus: 'success',
            errorMsg: null
        }
    };

}

class Register extends Component {
    render() {
        const AntWrappedRegisterForm = Form.create()(RegisterForm);
        return (
            <div className="auth-form">
                <h1 className="text-center">Register</h1>
                <div>
                    <AntWrappedRegisterForm/>
                </div>
            </div>
        );
    }
}


export default Register;
