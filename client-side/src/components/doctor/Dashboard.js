import {Component} from "react";
import React from "react";
import 'antd/dist/antd.css';
import '../../styles/dashboard.css';
import DashboardWorkingDay from "./DashboardWorkingDay";
import DashboardMenu from "./DashboardMenu";
import {Col, Row} from "antd";


class Dashboard extends Component {

    render() {
        return (
            <Row justify={'center'} type={'flex'}>
                    <Col sm={6} xs={24}>
                        <DashboardMenu/>
                    </Col>
                    <Col sm={18} xs={24}>
                        <DashboardWorkingDay/>
                    </Col>
            </Row>
        )
            ;
    }
}

export default Dashboard;
