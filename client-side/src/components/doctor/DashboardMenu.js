import {Component} from "react";
import React from "react";
import '../../styles/panelMenu.css';

class DashboardMenu extends Component {
    render() {
        return (
            <div className={'panel-menu'}>
                <ul>
                    <li><a href='/modifyDay'>Add new day</a></li>
                    <li><a href='/modifyDay'>Modify Day</a></li>
                </ul>
            </div>
        );
    }
}

export default DashboardMenu;
