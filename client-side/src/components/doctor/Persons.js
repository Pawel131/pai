import {Component} from "react";
import React from "react";
import {persons} from '../../common/APIUtils';

class Persons extends Component {
    constructor(props) {
        super(props);
        this.state = {
            persons: [],
        };
    }

    fetchData() {
        persons().then(persons => {
                persons.forEach(person => this.setState({persons: [...this.state.persons, person]}))
            }
        );
    }

    componentDidMount() {
        this.fetchData();
    }


    personTable() {
        const {persons} = this.state;
        return (
            <div>
                <table className={'table'}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>full name</th>
                        <th>email</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        persons.map(
                            (person, key) =>
                                <tr key={key} className={'text-left'}>
                                    <td>{key}</td>
                                    <td>{person.fullName}</td>
                                    <td>{person.email}</td>
                                </tr>
                        )
                    }
                    </tbody>
                </table>
            </div>
        );
    }

    render() {
        return (
            this.personTable()
        );
    }
}

export default Persons;