import {Component} from "react";
import React from "react";
import {Col, DatePicker, Row, Table} from 'antd';
import moment from 'moment';
import 'antd/dist/antd.css';
import '../../styles/dashboardWorkingDay.css';
import {DATE_FORMAT} from "../../common/constants";
import {notification} from "antd/lib/index";
import {workingDayVisits} from "../../common/APIUtils";


const columns = [{
    title: 'Start',
    dataIndex: 'start',
}, {
    title: 'Stop',
    dataIndex: 'stop',
}];

class DashboardWorkingDay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            periodsInTable: [],
            visits: [],
            dayDate: null
        };

        this.getData = this.getData.bind(this);
        this.postData = this.postData.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.expandedRowRender = this.expandedRowRender.bind(this);
    }


    expandedRowRender(start, stop) {
        const {visits} = this.state;
        const columns = [
            {title: 'hour', dataIndex: 'hour'},
            {title: 'patient', dataIndex: 'patient'}];

        // const data = visits;


        const data = visits.filter(visit =>
            moment(start, 'h:mm').isSameOrBefore(moment(visit.hour, 'h:mm'))
        ).filter(visit =>
            moment(stop, 'h:mm').isAfter(moment(visit.hour, 'h:mm'))
        );
        return (
            <Table
                columns={columns}
                dataSource={data}
                pagination={false}
            />
        )
            ;
    }

    getData(date) {
        workingDayVisits(date).then(data => {
            let periods = [];
            let visits = [];

            data.periods.forEach(period => periods.push({
                start: period.start,
                stop: period.finish,
                key: Math.random()
            }));

            data.visits.forEach(visit => visits.push({
                patient: visit.patientName,
                hour: visit.startHour,
                key: Math.random()
            }));


            this.setState({
                periodsInTable: periods,
                dayDate: moment(data.date, 'YYYY-MM-DD'),
                visits: visits
            });
        }).catch(error => {
            notification.error({
                message: 'Modify Working Day Error',
                description: error.message,
            });
        })
    }

    postData() {
        const data = {
            date: this.state.dayDate.format("YYYY-MM-DD")
        };
        this.getData(data);
    }

    handleDateChange(dateInObject) {
        this.setState({
            dayDate: dateInObject,
        }, this.postData);
    }

    componentWillMount() {
        const data = {
            date: moment().format("YYYY-MM-DD")
        };
        this.getData(data);
    }

    render() {
        const {periodsInTable, dayDate} = this.state;
        return (
            <Row justify={'center'} type={'flex'}>
                <Col xs={22} sm={22}>
                    <div className={"dashboard-working-day"}>
                        <DatePicker size={'large'}
                                    format={DATE_FORMAT}
                                    value={dayDate}
                                    onChange={this.handleDateChange}/>
                        <div className={'margin-top-20'}>
                            <Table
                                expandedRowRender={record =>
                                    this.expandedRowRender(record.start, record.stop)
                                }
                                pagination={false}
                                className="components-table-demo-nested"
                                columns={columns}
                                dataSource={periodsInTable}/>
                        </div>
                    </div>
                </Col>
            </Row>
        );
    }
}

export default DashboardWorkingDay;
