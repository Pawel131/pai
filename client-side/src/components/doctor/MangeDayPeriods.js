import {Component} from "react";
import React from "react";
import {Form, Button, notification, Popconfirm, Row, Col} from 'antd';
import {DatePicker, Table, Input} from 'antd';
import moment from 'moment';
import 'antd/dist/antd.css';
import '../../styles/managePeriods.css';
import {modifyDay} from "../../common/APIUtils";
import {DATE_FORMAT} from "../../common/constants";
import {Redirect} from "react-router";

const FormItem = Form.Item;

const StartCell = ({editable, value, record, onChange}) => (
    <div>
        {editable
            ?
            <FormItem
                validateStatus={record.validateStartStatus}
                help={record.errorStartMsg}
            >
                <Input value={value} onChange={e => onChange(e.target.value)}/>
            </FormItem>
            : value
        }
    </div>
);
const StopCell = ({editable, value, record, onChange}) => (
    <div>
        {editable
            ?
            <FormItem
                validateStatus={record.validateStopStatus}
                help={record.errorStopMsg}
            >
                <Input value={value} onChange={e => onChange(e.target.value)}/>
            </FormItem>
            : value
        }
    </div>
);

class ManageDayPeriodsForm extends Component {
    constructor(props) {
        super(props);
        this.columns = [{
            title: 'Start',
            dataIndex: 'Start',
            render: (text, record) => this.renderColumns(text, record, 'Start'),
        }, {
            title: 'Stop',
            dataIndex: 'Stop',
            render: (text, record) => this.renderColumns(text, record, 'Stop'),
        }, {
            title: 'operation',
            dataIndex: 'operation',
            render: (text, record) => {
                const {editable} = record;
                return (
                    <div className="editable-row-operations">
                        {
                            editable ?
                                <span>
                                    <a onClick={() => this.save(record.key)}>Save</a>
                                    <Popconfirm title="Sure to cancel?" onConfirm={() => this.cancel(record.key)}>
                                        <a>Cancel</a>
                                    </Popconfirm>
                                </span>
                                : <a onClick={() => this.edit(record.key)}>Edit</a>
                        }
                    </div>
                );
            }
        }];

        this.state = {
            dayDate: {value: ''},
            periodsInTable: [],
            startTime: null,
            stopTime: null,
            count: 0,
            dashboardRedirect: false
        };

        this.cacheData = this.state.periodsInTable.map(item => ({...item}));

        this.postData = this.postData.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.isFormInvalid = this.isFormInvalid.bind(this);
        this.assignPeriod = this.assignPeriod.bind(this);
    }

    edit(key) {
        const newData = [...this.state.periodsInTable];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            target.editable = true;
            this.setState({periodsInTable: newData});
        }
    }

    checkIsValid(data) {
        if (data && data.validateStartStatus === 'success' && data.validateStopStatus === 'success') {
            return true;
        } else {
            notification.error({
                message: 'Modify Working Day Error',
                description: "sth went wrong! ",
            });
            return false;
        }
    }

    save(key) {
        const newData = [...this.state.periodsInTable];
        const target = newData.filter(item => key === item.key)[0];
        if (this.checkIsValid(target)) {
            delete target.editable;
            this.setState({periodsInTable: newData});
            this.cacheData = newData.map(item => ({...item}));
        }
    }

    cancel(key) {
        const newData = [...this.state.periodsInTable];
        const target = newData.filter(item => key === item.key)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => key === item.key)[0]);
            delete target.editable;
            this.setState({data: newData});
        }
    }

    renderColumns(text, record, column) {
        if (column === 'Start') {
            return (
                <StartCell
                    editable={record.editable}
                    value={text}
                    record={record}
                    onChange={value => this.handleInputChange(value, record, column)}
                />
            );
        } else {
            return (
                <StopCell
                    editable={record.editable}
                    value={text}
                    record={record}
                    onChange={value => this.handleInputChange(value, record, column)}
                />
            );
        }
    }

    handleInputChange(value, record, column) {
        const newData = [...this.state.periodsInTable];
        const key = record.key;
        let target = newData.filter(item => key === item.key)[0];
        if (target) {
            target[column] = value;
            this.formatValidate(target, column);
            this.setState({periodsInTable: newData});
        }
    }

    handleDateChange(dateInObject) {
        this.setState({
            dayDate: {
                value: dateInObject,
                ...this.dateValidate(dateInObject)
            }
        });
    }

    isFormInvalid() {
        return !(this.state.dayDate.validateStatus === 'success');
    }

    assignPeriod() {
        const {count} = this.state;
        const period = {
            Start: null,
            Stop: null,
            key: count
        };
        this.setState({
            periodsInTable: [...this.state.periodsInTable, period],
            count: count + 1
        });

    }

    postData(event) {
        event.preventDefault();
        const {dayDate, periodsInTable} = this.state;

        const periods = periodsInTable.map((period) => (
            {'start': period.Start, 'finish': period.Stop})
        );
        const workingDayData = {
            dayDate: dayDate.value.format("YYYY-MM-DD"),
            periods: periods
        };
        modifyDay(workingDayData).then(data => {
            notification.success({
                message: 'Success',
                description: data.description,
            });
            this.setState({dashboardRedirect: true})
        }).catch(error => {
            notification.error({
                message: 'Modify Working Day Error',
                description: error.message,
            });
        })
    }

    render() {
        const {dayDate, periodsInTable, dashboardRedirect} = this.state;
        const columns = this.columns;
        return (
            <div className={'manage-periods'}>
                <Button icon={"plus"} onClick={this.assignPeriod} type={"primary"}/>
                <Row>
                    <Table
                        pagination={false}
                        columns={columns}
                        dataSource={periodsInTable}/>
                </Row>
                <Row>
                    <div className={"margin-top-20"}>
                        <Form layout={'inline'} onSubmit={this.postData}>
                            <Col sm={6} xs={24}>
                                <FormItem
                                    validateStatus={dayDate.validateStatus}
                                    className={'width-for-mobile-100'}
                                    help={dayDate.errorMsg}>
                                    <DatePicker size={'large'}
                                                format={DATE_FORMAT}
                                                value={dayDate.value}
                                                className={'width-for-mobile-100'}
                                                onChange={this.handleDateChange}/>
                                </FormItem>
                            </Col>
                            <Col sm={18} xs={24}>
                                <Button size={'large'}
                                        htmlType={'submit'}
                                        icon={"save"}
                                        className={'width-for-mobile-100'}
                                        disabled={this.isFormInvalid()}>Save</Button>
                            </Col>
                        </Form>
                    </div>
                    {dashboardRedirect && (
                        <Redirect to={'/dashboard'}/>
                    )}
                </Row>
            </div>

        );
    }

    dateValidate = (date) => {
        if (!date) {
            return {
                validateStatus: 'error',
                errorMsg: 'Date may not be empty'
            }
        }
        if (date.diff(moment(), 'days') < 1) {
            return {
                validateStatus: 'error',
                errorMsg: 'Date may be from future'
            }
        }
        return {
            validateStatus: 'success',
            errorMsg: null
        }
    };

    formatValidate = (record, column) => {
        const startValue = record[column];
        if (!startValue) {
            this.setEmptyMsg(record, column);
        } else {
            const FORMAT_REGEX = RegExp('^([0-9]{2}):([0-9]{2})$');
            if (!FORMAT_REGEX.test(startValue)) {
                this.setFormatMsg(record, column);
            } else {
                this.setSuccessMsg(record, column);
            }
        }

    };

    setEmptyMsg = (record, column) => {
        if (column === 'Start') {
            record.validateStartStatus = 'error';
            record.errorStartMsg = 'Start time may not be empty';
        } else {
            record.validateStopStatus = 'error';
            record.errorStopMsg = 'Stop time may not be empty';
        }
    };

    setSuccessMsg = (record, column) => {
        if (column === 'Start') {
            record.validateStartStatus = 'success';
            record.errorStartMsg = null;
        } else {
            record.validateStopStatus = 'success';
            record.errorStopMsg = null;
        }
    };

    setFormatMsg = (record, column) => {
        if (column === 'Start') {
            record.validateStartStatus = 'error';
            record.errorStartMsg = 'Start Format not valid';
        } else {
            record.validateStopStatus = 'error';
            record.errorStopMsg = 'Stop format not valid';
        }
    };
}

class ManageDayPeriods
    extends Component {
    render() {
        const AntWrappedManageDayPeriodsForm = Form.create()(ManageDayPeriodsForm);
        return (
            <div className="container">
                <h1 className="text-center">Working Day Definition</h1>
                <div>
                    <AntWrappedManageDayPeriodsForm/>
                </div>
            </div>
        );
    }
}

export default ManageDayPeriods;
