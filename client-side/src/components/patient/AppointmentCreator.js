import {Component} from "react";
import React from "react";
import {Col, DatePicker, Popconfirm, Row, Table} from 'antd';
import moment from 'moment';
import 'antd/dist/antd.css';
import {DATE_FORMAT} from "../../common/constants";
import {notification} from "antd/lib/index";
import {freeHoursInDate, confirmVisit} from "../../common/APIUtils";
import '../../styles/visit.css';
import {Redirect} from "react-router";

class AppointmentCreator extends Component {

    constructor(props) {
        super(props);

        this.columns = [{
            title: 'Free hour',
            dataIndex: 'hour',
        }, {
            dataIndex: 'operation',
            render: (text, record) => {
                return (
                    <Popconfirm title="Sure to confirm this hour?" onConfirm={() => this.confirmVisit(record.hour)}>
                        <a>Confirm visit</a>
                    </Popconfirm>
                );
            }
        }];

        this.state = {
            freeHours: [],
            dayDate: null,
            visitsRedirect: false
        };
        this.getData = this.getData.bind(this);
        this.postData = this.postData.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.confirmVisit = this.confirmVisit.bind(this);
    }

    confirmVisit(hour) {
        const data = {
            date: this.state.dayDate,
            startHour: hour
        };

        confirmVisit(data).then(data => {
                notification.success({
                    message: 'Success',
                    description: data.description
                });
                this.setState({visitsRedirect: true});
            }
        ).catch(error => {
            notification.error({
                message: 'Confirm Visit Error',
                description: error.message,
            });
        })

    }

    getData(date) {
        freeHoursInDate(date).then(data => {
            let hours = [];

            data.forEach(hour => hours.push({
                hour: hour,
                key: Math.random()
            }));

            this.setState({
                freeHours: hours
            });
        }).catch(error => {
            notification.error({
                message: 'Error',
                description: error.message || "Please contact with administrator",
            });
        })
    }

    postData() {
        const data = {
            date: this.state.dayDate.format("YYYY-MM-DD")
        };
        this.getData(data);
    }

    handleDateChange(dateInObject) {
        this.setState({
            dayDate: dateInObject,
        }, this.postData);


    }

    componentWillMount() {
        const data = {
            date: moment().format("YYYY-MM-DD")
        };
        this.setState({
            dayDate: moment()
        });
        this.getData(data);
    }

    render() {
        const {freeHours, dayDate, visitsRedirect} = this.state;
        const columns = this.columns;
        return (
            <Row justify={'center'} type={'flex'}>
                <Col span={20}>
                    <DatePicker size={'large'}
                                format={DATE_FORMAT}
                                value={dayDate}
                                className={"margin-bottom-20"}
                                onChange={this.handleDateChange}/>
                    <Row>
                        <Table pagination={{pageSize: 5}} columns={columns} dataSource={freeHours}/>
                    </Row>
                </Col>
                {visitsRedirect && (
                    <Redirect to={'/visits'}/>
                )}
            </Row>
        );
    }
}

export default AppointmentCreator;
