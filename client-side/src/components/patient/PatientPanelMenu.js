import {Component} from "react";
import React from "react";
import '../../styles/panelMenu.css';
import {Row} from "antd";

class PatientPanelMenu extends Component {
    render() {
        return (
            <Row>
                <div className={'panel-menu'}>
                    <ul>
                        <li><a href='/visits'>My visits</a></li>
                        <li><a href='/visit'>New visit</a></li>
                    </ul>
                </div>
            </Row>
        );
    }
}

export default PatientPanelMenu;
