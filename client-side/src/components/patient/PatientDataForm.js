import {Component} from "react";
import React from "react";
import {Form, Input, Button, notification, Row, Col} from 'antd';
import {changePersonalData, getPersonalData} from '../../common/APIUtils'
import 'antd/dist/antd.css';
import '../../styles/PatientDataForm.css';

const FormItem = Form.Item;

class PatientDataForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: null,
            email: null,
            name: {value: ''},
            surname: {value: ''},
            country: {value: ''},
            city: {value: ''},
            postalCode: {value: ''},
            street: {value: ''},
            streetNo: {value: ''}
        };
        this.postData = this.postData.bind(this);
        this.getData = this.getData.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.isFormInvalid = this.isFormInvalid.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const inputName = target.name;
        const inputValue = target.value;

        this.setState({
            [inputName]: {
                value: inputValue
                // ...validationFun(inputValue)
            }
        });
    }

    isFormInvalid() {
        return false;
    }

    getData() {
        getPersonalData().then(data => {
            this.setState({
                id: data.id,
                email: data.email,
                name: {value: data.name},
                surname: {value: data.surname},
                country: {value: data.country},
                street: {value: data.street},
                streetNo: {value: data.streetNo},
                city: {value: data.city},
                postalCode: {value: data.postalCode}
            });
        }).catch(error => {
            notification.error({
                message: 'Error',
                description: error.message || 'Something went wrong pleas try again',
            });
        })

    }

    componentWillMount() {
        this.getData();
    }

    postData(event) {
        event.preventDefault();
        const personalData = {
            id: this.state.id,
            email: this.state.email,
            name: this.state.name.value,
            surname: this.state.surname.value,
            country: this.state.country.value,
            city: this.state.city.value,
            postalCode: this.state.postalCode.value,
            street: this.state.street.value,
            streetNo: this.state.streetNo.value
        };
        changePersonalData(personalData).then(data => {
            notification.success({
                message: 'Success',
                description: data.description,
            });
        }).catch(error => {
            notification.error({
                message: 'Error',
                description: error.message || 'Something went wrong please try again',
            });
        })
    }

    render() {
        const {name, surname, country, city, postalCode, streetNo, street} = this.state;
        return (
            <Row justify={'center'} type={'flex'}>
                <Col xs={22} sm={22}>
                    <Form onSubmit={this.postData}>
                        <Col sm={12} xs={24}>
                            <FormItem
                                label={'Name'}
                                colon={false}>
                                <Input
                                    value={name.value}
                                    name={'name'}
                                    onChange={this.handleInputChange}
                                    placeholder="Name"/>
                            </FormItem>
                        </Col>
                        <Col sm={12} xs={24}>
                            <FormItem
                                label={'Surname'}
                                colon={false}>
                                <Input
                                    value={surname.value}
                                    name={'surname'}
                                    onChange={this.handleInputChange}
                                    placeholder="Surname"/>
                            </FormItem>
                        </Col>
                        <Col span={24}>
                            <FormItem
                                label={'Country'}
                                colon={false}>
                                <Input
                                    value={country.value}
                                    name={'country'}
                                    onChange={this.handleInputChange}
                                    placeholder="Country"/>
                            </FormItem>
                        </Col>
                        <Col sm={12} xs={24}>
                            <FormItem
                                label={'City'}
                                colon={false}>
                                <Input
                                    value={city.value}
                                    name={'city'}
                                    onChange={this.handleInputChange}
                                    placeholder="City"/>
                            </FormItem>
                        </Col>
                        <Col sm={12} xs={24}>
                            <FormItem label={'Postal Code'} colon={false}>
                                <Input
                                    value={postalCode.value}
                                    name={'postalCode'}
                                    onChange={this.handleInputChange}
                                    placeholder="Postal code"/>
                            </FormItem>
                        </Col>
                        <Col sm={12} xs={24}>
                            <FormItem label={'Street'} colon={false}>
                                <Input
                                    value={street.value}
                                    name={'street'}
                                    onChange={this.handleInputChange}
                                    placeholder="Street"/>
                            </FormItem>
                        </Col>
                        <Col sm={12} xs={24}>
                            <FormItem label={'Street no.'} colon={false}>
                                <Input
                                    value={streetNo.value}
                                    onChange={this.handleInputChange}
                                    name={'streetNo'}
                                    placeholder="Street no"/>
                            </FormItem>
                        </Col>
                        <Col span={24} className={"text-center"}>
                            <Button
                                htmlType="submit"
                                type={"primary"}
                                disabled={this.isFormInvalid()}
                                size={'large'}>
                                Save
                            </Button>
                        </Col>
                    </Form>
                </Col>
            </Row>
        );
    }
}

class PatientDataFormWrapper
    extends Component {
    render() {
        const AntWrappedPatientDataForm = Form.create()(PatientDataForm);
        return (
            <div>
                <h1 className="text-center">Personal Data</h1>
                <div className={"patient-form"}>
                    <AntWrappedPatientDataForm/>
                </div>
            </div>
        );
    }
}


export default PatientDataFormWrapper;
