import {Component} from "react";
import React from "react";
import PatientDataFormWrapper from "./PatientDataForm";
import PatientPanelMenu from "./PatientPanelMenu";
import {Col, Row} from "antd";
import '../../styles/patientPanel.css';


class PatientPanel extends Component {

    render() {
        return (
            <div className={'patient-panel'}>
                <Row justify={'center'} type={'flex'}>
                    <Col sm={6} xs={24}>
                        <PatientPanelMenu/>
                    </Col>
                    <Col sm={18} xs={24}>
                        <PatientDataFormWrapper/>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default PatientPanel;
