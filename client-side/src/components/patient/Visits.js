import {Component} from "react";
import React from "react";
import {Col, Row, Table} from 'antd';
import 'antd/dist/antd.css';
import '../../styles/dashboardWorkingDay.css';
import {notification} from "antd/lib/index";
import {visits} from "../../common/APIUtils";


const columns = [{
    title: 'date',
    dataIndex: 'date',
}, {
    title: 'time',
    dataIndex: 'time',
}];

class Visits extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visits: [],
        };

        this.getData = this.getData.bind(this);
    }

    getData() {
        visits().then(data => {
            let visits = [];

            data.forEach(visit => visits.push({
                date: visit.date,
                time: visit.time,
                key: Math.random()
            }));

            this.setState({
                visits: visits
            });
        }).catch(error => {
            notification.error({
                message: 'Modify Working Day Error',
                description: error.message,
            });
        })
    }


    componentWillMount() {
        this.getData();
    }

    render() {
        const {visits} = this.state;
        return (
            <Row justify={'center'} type={'flex'}>
                <Col span={14}>
                    <Table columns={columns} dataSource={visits}/>
                </Col>
            </Row>
        );
    }
}

export default Visits;
