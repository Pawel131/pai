import {API_BASE_URL, ACCESS_TOKEN} from '../common/constants';

const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json',
    });

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (response.ok) {
                    return json;
                } else {
                    return Promise.reject(json);
                }
            })
        );
};

export function login(data) {
    return request({
        url: API_BASE_URL + "/login",
        method: 'POST',
        body: JSON.stringify(data)
    });
}

export function register(data) {
    return request({
        url: API_BASE_URL + "/register",
        method: 'POST',
        body: JSON.stringify(data)
    });
}

export function changePersonalData(data) {
    return request({
        url: API_BASE_URL + "/changePersonalData",
        method: 'POST',
        body: JSON.stringify(data)
    });
}

export function getPersonalData() {
    return request({
        url: API_BASE_URL + "/getPersonalData",
        method: 'GET'
    });
}

export function modifyDay(data) {
    return request({
        url: API_BASE_URL + "/modifyWorkingDay",
        method: 'POST',
        body: JSON.stringify(data)
    });
}

export function persons() {
    return request({
        url: API_BASE_URL + "/persons",
        method: 'GET'
    });
}

export function visits() {
    return request({
        url: API_BASE_URL + "/visits",
        method: 'GET'
    });
}

export function workingDayVisits(data) {
    return request({
        url: API_BASE_URL + "/workingDayVisits",
        method: 'POST',
        body: JSON.stringify(data)
    });
}

export function freeHoursInDate(data) {
    return request({
        url: API_BASE_URL + "/getFreeHoursInDate",
        method: 'POST',
        body: JSON.stringify(data)
    });
}

export function confirmVisit(data) {
    return request({
        url: API_BASE_URL + "/confirmVisit",
        method: 'POST',
        body: JSON.stringify(data)
    });
}