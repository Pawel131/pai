import {BrowserRouter, Route} from 'react-router-dom'
import React from 'react';

import Login from "../components/account/Login";
import Register from "../components/account/Register";
import Persons from "../components/doctor/Persons";
import Dashboard from "../components/doctor/Dashboard";
import ModifyDayPeriods from "../components/doctor/MangeDayPeriods";
import PatientPanel from "../components/patient/PatientPanel";
import AppointmentCreator from "../components/patient/AppointmentCreator";
import Visits from "../components/patient/Visits";

export default () => (
    <BrowserRouter>
        <div>
            <Route path={"/"} exact component={Login}/>
            <Route path={"/persons"} exact component={Persons}/>
            <Route path={"/modifyDay"} exact component={ModifyDayPeriods}/>
            <Route path={"/dashboard"} exact component={Dashboard}/>
            <Route path={"/register"} exact component={Register}/>
            <Route path={"/patientPanel"} exact component={PatientPanel}/>
            <Route path={"/visit"} exact component={AppointmentCreator}/>
            <Route path={"/visits"} exact component={Visits}/>
        </div>
    </BrowserRouter>

);