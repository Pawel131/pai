export const API_BASE_URL = 'http://localhost:8080';
export const ACCESS_TOKEN = 'accessToken';

export const DATE_FORMAT = 'YYYY/MM/DD';