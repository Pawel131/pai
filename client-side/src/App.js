import React, {Component} from 'react';
import './styles/App.css';
import Router from "./common/Router";

class App extends Component {
    render() {
        return (
            <div>
                <div className={'app-header'}><h1>Dental Clinic</h1></div>
                <Router/>
            </div>
        );
    }
}

export default App;
