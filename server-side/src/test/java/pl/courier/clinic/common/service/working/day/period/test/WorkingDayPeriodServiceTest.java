package pl.courier.clinic.common.service.working.day.period.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.courier.clinic.dental.model.WorkingPeriod;
import pl.courier.clinic.dental.model.repository.WorkingPeriodDayRepository;
import pl.courier.clinic.dental.service.business.logic.WorkingDayPeriodServiceImpl;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class WorkingDayPeriodServiceTest {

    @Mock
    private WorkingPeriodDayRepository workingPeriodDayRepository;

    @InjectMocks
    private WorkingDayPeriodServiceImpl workingDayPeriodService;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testWorkingDayPeriodsWithoutVisits() {

        when(workingPeriodDayRepository.findWorkingPeriodInDate(any())).thenReturn(setupData());

        List<LocalTime> workingPeriodsAndAssemblyToWorkingHours = workingDayPeriodService
                .findWorkingPeriodsAndAssemblyToWorkingHours(LocalDate.now());

        assertThat(workingPeriodsAndAssemblyToWorkingHours.size(), is(12));

    }

    private List<WorkingPeriod> setupData() {
        List<WorkingPeriod> data = new ArrayList<>();
        data.add(new WorkingPeriod(LocalTime.of(12, 0), LocalTime.of(15, 0)));
        data.add(new WorkingPeriod(LocalTime.of(17, 0), LocalTime.of(20, 0)));

        return data;
    }
}
