package pl.courier.clinic.common.service.planned.visit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.courier.clinic.clinic.model.PlannedVisit;
import pl.courier.clinic.dental.model.repository.PlannedVisitRepository;
import pl.courier.clinic.dental.service.business.PlannedVisitType;
import pl.courier.clinic.dental.service.PlannedVisitFactory;
import pl.courier.clinic.dental.service.business.logic.PlannedVisitServiceImpl;
import pl.courier.clinic.dental.service.business.logic.WorkingDayPeriodService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class PlannedVisitServiceTest {
    @Mock
    private PlannedVisitRepository plannedVisitRepository;

    @Mock
    private WorkingDayPeriodService workingDayPeriodService;


    @InjectMocks
    private PlannedVisitServiceImpl plannedVisitService;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void workingDayPeriodsWithoutVisitsTest() {

        when(plannedVisitRepository.findAllInDay(any())).thenReturn(setupData());

        List<LocalTime> plannedVisits = plannedVisitService.getPlannedVisitHours(LocalDate.now());
        assertThat(plannedVisits.size(), is(6));

    }

    @Test
    public void getFreeHoursInDayTest() {
        when(plannedVisitRepository.findAllInDay(any())).thenReturn(setupData());
        when(workingDayPeriodService.findWorkingPeriodsAndAssemblyToWorkingHours(any())).thenReturn(setupDataForWorkingPeriodsInDay());

        List<String> freeHoursInDay = plannedVisitService.getFreeHoursInDay(LocalDate.now());
        assertThat(freeHoursInDay.size(), is(5));

    }

    private List<PlannedVisit> setupData() {
        List<PlannedVisit> data = new ArrayList<>();
        data.add(PlannedVisitFactory.create(LocalTime.of(12, 0), PlannedVisitType.NORMAL));
        data.add(PlannedVisitFactory.create(LocalTime.of(12, 30), PlannedVisitType.NORMAL));
        data.add(PlannedVisitFactory.create(LocalTime.of(14, 0), PlannedVisitType.REPAIR));
        data.add(PlannedVisitFactory.create(LocalTime.of(15, 0), PlannedVisitType.WHITE));

        return data;
    }

    private List<LocalTime> setupDataForWorkingPeriodsInDay() {
        List<LocalTime> data = new ArrayList<>();

        data.add(LocalTime.of(12, 0));
        data.add(LocalTime.of(12, 30));
        data.add(LocalTime.of(13, 0));
        data.add(LocalTime.of(13, 30));
        data.add(LocalTime.of(14, 0));
        data.add(LocalTime.of(14, 30));
        data.add(LocalTime.of(15, 0));
        data.add(LocalTime.of(15, 30));
        data.add(LocalTime.of(16, 0));
        data.add(LocalTime.of(16, 30));
        data.add(LocalTime.of(17, 0));

        return data;
    }
}
