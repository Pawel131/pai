package pl.courier.clinic.TimeTest;

import org.junit.Test;
import pl.courier.clinic.dental.model.WorkingPeriod;
import pl.courier.clinic.common.service.TimeInComponents;
import pl.courier.clinic.common.service.TimeUtils;

import java.time.LocalTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TimePeriodTest {

    @Test
    public void timePeriodTest() {
        WorkingPeriod workingPeriod = new WorkingPeriod();
        workingPeriod.setStartTime(LocalTime.of(12, 15));
        workingPeriod.setFinishTime(LocalTime.of(15, 0));
        long workingSeconds = workingPeriod.getWorkingSeconds();
        TimeInComponents timeInComponents = TimeUtils.convertTime(workingSeconds);

        assertThat(timeInComponents.getHours(), is(2L));
        assertThat(timeInComponents.getMinutes(), is(45L));

    }

}
