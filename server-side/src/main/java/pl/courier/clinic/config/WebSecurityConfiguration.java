package pl.courier.clinic.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import pl.courier.clinic.common.security.TokenUtil;
import pl.courier.clinic.customer.service.bussines.logic.UserProvider;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserProvider userProvider;
    private final TokenUtil tokenUtil;

    @Value("${token.prefix}")
    public String tokenPrefix;

    @Value("${token.header}")
    public String headerString;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/login", "/register", "/authenticate", "/getPeriods").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), userProvider, tokenUtil, tokenPrefix, headerString))
                .csrf().disable();


    }

}
