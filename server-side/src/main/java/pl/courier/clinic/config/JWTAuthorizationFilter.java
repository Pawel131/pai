package pl.courier.clinic.config;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import pl.courier.clinic.common.security.TokenUtil;
import pl.courier.clinic.customer.service.bussines.logic.UserProvider;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final UserProvider userProvider;
    private final TokenUtil tokenUtil;

    private final String tokenPrefix;
    private final String headerString;

    JWTAuthorizationFilter(AuthenticationManager authManager, UserProvider userProvider,
                           TokenUtil tokenUtil, String tokenPrefix, String headerString) {
        super(authManager);
        this.userProvider = userProvider;
        this.headerString = headerString;
        this.tokenUtil = tokenUtil;
        this.tokenPrefix = tokenPrefix;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(headerString);

        if (header == null || !header.startsWith(tokenPrefix)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(headerString);
        if (token != null) {
            // parse the token.
            String email = tokenUtil.getUserEmail(token);
            if (email != null) {
                return new UsernamePasswordAuthenticationToken(userProvider.loadUserByUsername(email), null, new ArrayList<>());
            }
            return null;
        }
        return null;
    }
}