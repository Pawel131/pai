package pl.courier.clinic.customer.service.bussines.logic.authenticate;

import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.common.model.UserRole;
import pl.courier.clinic.common.security.TokenUtil;
import pl.courier.clinic.customer.model.LoginDetails;
import pl.courier.clinic.customer.model.Person;
import pl.courier.clinic.customer.model.repository.LoginDetailsRepository;
import pl.courier.clinic.customer.model.repository.PersonRepository;
import pl.courier.clinic.customer.service.assembler.LoginDetailsAssembler;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;
import pl.courier.clinic.customer.service.validator.AccountValidator;
import pl.courier.clinic.dental.service.business.logic.PatientService;

@Component
@AllArgsConstructor
public class Register extends AbstractAuthenticate {
    private final LoginDetailsRepository loginDetailsRepository;
    private final PersonRepository personRepository;
    private final AccountValidator accountValidator;
    private final PasswordEncoder passwordEncoder;
    private final PatientService patientService;
    private final TokenUtil tokenUtil;

    @Override
    @Transactional
    public String getEmailFromUser(LoginDetailsDto loginDetailsDto) throws ValidatorException {
        String email = loginDetailsDto.getEmail();
        accountValidator.isEmailUnique(email);
        setEncodedPassword(loginDetailsDto);
        LoginDetails loginDetails = LoginDetailsAssembler.toEntity(loginDetailsDto);

        Person person = personRepository.save(loginDetails.getPerson());
        loginDetailsRepository.save(loginDetails);
        patientService.save(person.getId());
        return email;
    }

    private void setEncodedPassword(LoginDetailsDto loginDetailsDto) {
        loginDetailsDto.setPassword(passwordEncoder.encode(loginDetailsDto.getPassword()));
    }

    @Override
    String generateToken(String email) {
        return tokenUtil.generateToken(email, UserRole.PATIENT.getDisplayName());
    }
}
