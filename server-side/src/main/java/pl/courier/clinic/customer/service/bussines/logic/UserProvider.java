package pl.courier.clinic.customer.service.bussines.logic;

import org.springframework.security.core.userdetails.UserDetailsService;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;

public interface UserProvider extends UserDetailsService {

    String getUserEmailByToken(String token);

}
