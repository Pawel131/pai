package pl.courier.clinic.customer.service.bussines.logic.authenticate;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.customer.service.bussines.logic.UserProvider;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;
import pl.courier.clinic.customer.service.validator.AccountValidator;

@Component
@AllArgsConstructor
public class AuthenticateUtils {
    private final AccountValidator accountValidator;
    private final UserProvider userProvider;

    void authenticate(LoginDetailsDto dto) throws ValidatorException {
        if (dto.getEmail() == null) {
            throw new ValidatorException("Empty email");
        }
        String email = dto.getEmail();
        UserDetails userDetails = userProvider.loadUserByUsername(email);

        accountValidator.checkIsPasswordCorrectAndThrowException(dto, userDetails);
    }


}
