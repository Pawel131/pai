package pl.courier.clinic.customer.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;

@Getter
@Setter
@Embeddable
public class Address {

    private String country;
    private String street;
    private String streetNo;
    private String city;
    private String postalCode;
}
