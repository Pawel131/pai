package pl.courier.clinic.customer.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.courier.clinic.customer.model.LoginDetails;

import java.util.Optional;

@Repository
public interface LoginDetailsRepository extends CrudRepository<LoginDetails, Long> {

    @Query("SELECT ld "
            + "FROM LoginDetails ld "
            + "JOIN FETCH ld.person p "
            + "WHERE p.email like :email")
    Optional<LoginDetails> findByEmail(@Param("email") String email);

}
