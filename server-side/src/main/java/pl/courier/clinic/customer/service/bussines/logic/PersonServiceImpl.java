package pl.courier.clinic.customer.service.bussines.logic;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.courier.clinic.customer.model.repository.PersonRepository;
import pl.courier.clinic.customer.service.assembler.PersonAssembler;
import pl.courier.clinic.customer.service.dto.PersonDto;
import pl.courier.clinic.customer.service.dto.PersonalDataDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Override
    @Transactional(readOnly = true)
    public List<PersonDto> findAll() {

        return personRepository.findAll().stream().map(PersonAssembler::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public PersonalDataDto findOne(String email) {
        return PersonAssembler.toPersonalDataDto(personRepository.findOneByEmail(email));
    }

    @Override
    @Transactional
    public void save(PersonalDataDto dto) {
        personRepository.save(PersonAssembler.toEntity(dto));
    }
}
