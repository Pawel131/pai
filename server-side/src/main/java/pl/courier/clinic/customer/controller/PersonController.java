package pl.courier.clinic.customer.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import pl.courier.clinic.common.SuccessMessage;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.customer.model.LoginDetails;
import pl.courier.clinic.customer.service.bussines.logic.PersonService;
import pl.courier.clinic.customer.service.bussines.logic.authenticate.PersonSaveFacade;
import pl.courier.clinic.customer.service.dto.PersonDto;
import pl.courier.clinic.customer.service.dto.PersonalDataDto;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin
public class PersonController {
    private PersonService personService;
    private PersonSaveFacade personSaveFacade;

    @GetMapping("/persons")
    public ResponseEntity<List<PersonDto>> persons() {
        return ResponseEntity.ok(personService.findAll());
    }

    @GetMapping("/getPersonalData")
    public ResponseEntity<PersonalDataDto> getPersonalData(@AuthenticationPrincipal LoginDetails user) {
        return ResponseEntity.ok(personService.findOne(user.getUsername()));
    }

    @PostMapping("/changePersonalData")
    public ResponseEntity<SuccessMessage> changePersonalData(@RequestBody PersonalDataDto dto) throws ValidatorException {
        personSaveFacade.save(dto);
        return ResponseEntity.ok(new SuccessMessage("Data successfully changed"));
    }

}
