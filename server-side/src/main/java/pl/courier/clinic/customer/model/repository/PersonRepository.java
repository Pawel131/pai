package pl.courier.clinic.customer.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.courier.clinic.customer.model.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("select (count(p.email) = 0)"
            + "from Person  p "
            + "WHERE p.email like :email")
    boolean isEmailUnique(@Param("email") String email);

    Person findOneByEmail(String email);
}
