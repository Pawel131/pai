package pl.courier.clinic.customer.service.bussines.logic.authenticate;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.common.model.UserRole;
import pl.courier.clinic.common.security.TokenUtil;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;
import pl.courier.clinic.dental.service.validator.DoctorValidator;
import pl.courier.clinic.dental.service.validator.PatientValidator;

import javax.validation.Valid;

@Service
@AllArgsConstructor
public class Login extends AbstractAuthenticate {
    private final DoctorValidator doctorValidator;
    private final PatientValidator patientValidator;
    private final TokenUtil tokenUtil;

    @Override
    String getEmailFromUser(LoginDetailsDto dto) throws ValidatorException {
        String email = dto.getEmail();
        if (email == null || email.isEmpty()) {
            throw new ValidatorException("Email cannot be null or empty");
        }

        return email;
    }

    @Override
    String generateToken(String email) throws ValidatorException {
        if(doctorValidator.checkIsDoctor(email)) {
            return tokenUtil.generateToken(email, UserRole.DOCTOR.getDisplayName());
        } else if (patientValidator.checkIsPatient(email)) {
            return tokenUtil.generateToken(email, UserRole.PATIENT.getDisplayName());
        }

        throw new UsernameNotFoundException((String.format("User with email:%s is not exist in the system", email)));
    }
}
