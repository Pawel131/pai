package pl.courier.clinic.customer.service.validator;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.customer.service.dto.PersonalDataDto;

@Component
@AllArgsConstructor
public class PersonValidator {

    public void validate(PersonalDataDto dto) throws ValidatorException {
        if (isEmpty(dto.getName()) || isEmpty(dto.getSurname())) {
            throw new ValidatorException("Name and surname cannot be empty.");
        }
    }

    private boolean isEmpty(String str) {
        return str.isEmpty();
    }

}
