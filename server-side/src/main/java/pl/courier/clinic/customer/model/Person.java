package pl.courier.clinic.customer.model;

import lombok.Getter;
import lombok.Setter;
import pl.courier.clinic.common.model.AbstractEntity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "customer", name = "persons")
@Getter
@Setter
public class Person extends AbstractEntity {

    private static final long serialVersionUID = -5523696305910126885L;

    private String name;
    private String surname;
    private String email;

    @Embedded
    private Address address;

    public String getFullName() {
        return name + " " + surname;
    }


}
