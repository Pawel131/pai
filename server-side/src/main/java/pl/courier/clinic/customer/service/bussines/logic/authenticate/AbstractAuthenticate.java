package pl.courier.clinic.customer.service.bussines.logic.authenticate;

import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;

public abstract class AbstractAuthenticate {

    public final String templateAuthenticate(LoginDetailsDto dto) throws ValidatorException {
        return generateToken(getEmailFromUser(dto));
    }

    abstract String getEmailFromUser(LoginDetailsDto dto) throws ValidatorException;


    abstract String generateToken(String email) throws ValidatorException;
}
