package pl.courier.clinic.customer.service.assembler;

import pl.courier.clinic.customer.model.Address;
import pl.courier.clinic.customer.model.Person;
import pl.courier.clinic.customer.service.dto.PersonDto;
import pl.courier.clinic.customer.service.dto.PersonalDataDto;

public class PersonAssembler {

    public static PersonDto toDto(Person entity) {
        PersonDto dto = new PersonDto();
        dto.setEmail(entity.getEmail());
        dto.setFullName(entity.getName() + " " + entity.getSurname());
        return dto;
    }

    public static PersonalDataDto toPersonalDataDto(Person entity) {
        Address address = entity.getAddress();

        PersonalDataDto.Builder personBuilder = new PersonalDataDto.Builder()
                .id(entity.getId())
                .name(entity.getName())
                .surname(entity.getSurname())
                .email(entity.getEmail());
        if (address != null) {
            personBuilder.city(address.getCity())
                    .street(address.getStreet())
                    .streetNo(address.getStreetNo())
                    .country(address.getCountry())
                    .postalCode(address.getPostalCode());
        }
        return personBuilder.build();
    }

    public static Person toEntity(PersonalDataDto dto) {
        Person entity = new Person();
        Address address = new Address();

        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        entity.setSurname(dto.getSurname());

        address.setCity(dto.getCity());
        address.setStreet(dto.getStreet());
        address.setStreetNo(dto.getStreetNo());
        address.setCountry(dto.getCountry());
        address.setPostalCode(dto.getPostalCode());
        entity.setAddress(address);

        return entity;
    }

}
