package pl.courier.clinic.customer.service.assembler;


import pl.courier.clinic.customer.model.LoginDetails;
import pl.courier.clinic.customer.model.Person;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;

public class LoginDetailsAssembler {

    public static LoginDetailsDto toDto(LoginDetails entity) {
        LoginDetailsDto loginDetailsDto = new LoginDetailsDto();
        loginDetailsDto.setEmail(entity.getUsername());
        loginDetailsDto.setPassword(entity.getPassword());
        loginDetailsDto.setRoles(entity.getRoles());
        return loginDetailsDto;
    }

    public static LoginDetails toEntity(LoginDetailsDto dto) {
        LoginDetails loginDetails = new LoginDetails();
        Person person = new Person();
        person.setEmail(dto.getEmail());
        loginDetails.setPassword(dto.getPassword());
        loginDetails.setRoles(dto.getRoles());
        loginDetails.setPerson(person);
        return loginDetails;
    }
}
