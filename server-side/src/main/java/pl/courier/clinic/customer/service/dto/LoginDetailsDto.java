package pl.courier.clinic.customer.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LoginDetailsDto {

    private String email;
    private String password;
    private String roles;

    public LoginDetailsDto(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
