package pl.courier.clinic.customer.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.courier.clinic.common.service.dto.AbstractDto;

@Getter
@Setter
@NoArgsConstructor
public class PersonalDataDto extends AbstractDto {
    private String name;
    private String email;
    private String surname;
    private String country;
    private String street;
    private String streetNo;
    private String city;
    private String postalCode;


    private PersonalDataDto(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.email = builder.email;
        this.surname = builder.surname;
        this.country = builder.country;
        this.street = builder.street;
        this.streetNo = builder.streetNo;
        this.city = builder.city;
        this.postalCode = builder.postalCode;
    }


    public static class Builder {
        private Long id;
        private String name;
        private String email;
        private String surname;
        private String country;
        private String street;
        private String streetNo;
        private String city;
        private String postalCode;

        public Builder() {
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public Builder street(String street) {
            this.street = street;
            return this;
        }

        public Builder streetNo(String streetNo) {
            this.streetNo = streetNo;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder postalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }


        public PersonalDataDto build() {
            return new PersonalDataDto(this);
        }

    }


}
