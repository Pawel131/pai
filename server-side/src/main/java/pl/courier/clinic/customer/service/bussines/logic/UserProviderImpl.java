package pl.courier.clinic.customer.service.bussines.logic;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.common.security.TokenUtil;
import pl.courier.clinic.customer.model.LoginDetails;
import pl.courier.clinic.customer.model.repository.LoginDetailsRepository;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;
import pl.courier.clinic.dental.service.validator.PatientValidator;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserProviderImpl implements UserProvider {

    private final LoginDetailsRepository loginDetailsRepository;
    private final TokenUtil tokenUtil;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) {
        Optional<LoginDetails> loginDetailsOpt = loginDetailsRepository.findByEmail(email);
        if (loginDetailsOpt.isPresent()) {
            return loginDetailsOpt.get();
        } else {
            throw new UsernameNotFoundException(String.format("User with email:%s is not exist in the system", email));
        }
    }

    @Override
    public String getUserEmailByToken(String token) {
        return tokenUtil.getUserEmail(token);
    }

}
