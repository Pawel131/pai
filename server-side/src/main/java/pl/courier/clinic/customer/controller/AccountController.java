package pl.courier.clinic.customer.controller;


import lombok.AllArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.customer.service.bussines.logic.authenticate.Login;
import pl.courier.clinic.customer.service.bussines.logic.authenticate.Register;
import pl.courier.clinic.customer.service.bussines.logic.UserProvider;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;

@RestController
@AllArgsConstructor
@CrossOrigin
public class AccountController {

    private final String TOKEN = "token";
    private final UserProvider userProvider;
    private final Login login;
    private final Register register;

    @PostMapping("/authenticate")
    public String authenticate(@RequestBody String token) {
        return userProvider.getUserEmailByToken(token);
    }

    @PostMapping("/login")
    public String login(@RequestBody LoginDetailsDto loginDetailsDto) throws ValidatorException, JSONException {
        return getTokenAsJson(login.templateAuthenticate(loginDetailsDto));
    }

    @PostMapping("/register")
    public String register(@RequestBody LoginDetailsDto loginDetailsDto) throws ValidatorException, JSONException {
        return getTokenAsJson(register.templateAuthenticate(loginDetailsDto));
    }

    private String getTokenAsJson(String token) throws JSONException {
        JSONObject obj = new JSONObject();
        return obj.put(TOKEN, token).toString();
    }
}
