package pl.courier.clinic.customer.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDto {

    private String fullName;
    private String email;
}
