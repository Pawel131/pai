package pl.courier.clinic.customer.service.bussines.logic;

import pl.courier.clinic.customer.service.dto.PersonDto;
import pl.courier.clinic.customer.service.dto.PersonalDataDto;

import java.util.List;

public interface PersonService {

    List<PersonDto> findAll();

    PersonalDataDto findOne(String email);

    void save(PersonalDataDto dto);
}
