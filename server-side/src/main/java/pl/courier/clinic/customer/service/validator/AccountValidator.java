package pl.courier.clinic.customer.service.validator;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.customer.model.repository.PersonRepository;
import pl.courier.clinic.customer.service.dto.LoginDetailsDto;

@Component
@AllArgsConstructor
public class AccountValidator {

    private final PersonRepository personRepository;

    public void isEmailUnique(String email) throws ValidatorException {
        if (!personRepository.isEmailUnique(email)) {
            throw new ValidatorException("Email is exist. Please try with another one.");
        }
    }

    public void checkIsPasswordCorrectAndThrowException(LoginDetailsDto dto, UserDetails userDetails) throws ValidatorException {
        if (!BCrypt.checkpw(dto.getPassword(), userDetails.getPassword())) {
            throw new ValidatorException("Provided email or password are not correct");
        }
    }
}
