package pl.courier.clinic.customer.service.bussines.logic.authenticate;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.courier.clinic.common.exception.ValidatorException;
import pl.courier.clinic.customer.service.bussines.logic.PersonService;
import pl.courier.clinic.customer.service.dto.PersonalDataDto;
import pl.courier.clinic.customer.service.validator.PersonValidator;

@Service
@AllArgsConstructor
public class PersonSaveFacade {
    private final PersonValidator personValidator;
    private final PersonService personService;

    public void save(PersonalDataDto dto) throws ValidatorException {
        personValidator.validate(dto);
        personService.save(dto);
    }
}
