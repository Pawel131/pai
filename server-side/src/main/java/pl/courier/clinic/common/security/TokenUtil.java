package pl.courier.clinic.common.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Date;

@Component
public class TokenUtil {

    private static final long TOKEN_TIME = 864_000_000;
    @Value("${token.prefix}")
    private String prefix;

    @Value("${token.secret}")
    private String secret;

    public String getUserEmail(String token) {
        return Jwts.parser()
                .setSigningKey(secret.getBytes())
                .parseClaimsJws(token.replace(prefix + " ", ""))
                .getBody()
                .getSubject();
    }

    public String generateToken(String email, String role) {
        return Jwts.builder()
                .setSubject(email)
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_TIME))
                .signWith(SignatureAlgorithm.HS512, secret.getBytes())
                .claim("role", role)
                .compact();
    }

}
