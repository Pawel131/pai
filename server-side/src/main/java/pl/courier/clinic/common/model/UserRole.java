package pl.courier.clinic.common.model;

public enum UserRole {
    PATIENT("patient"),
    DOCTOR("doctor");

    String role;


    UserRole(String role) {
        this.role = role;
    }

    public String getDisplayName() {
        return role;
    }
}
