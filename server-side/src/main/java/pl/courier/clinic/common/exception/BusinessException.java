package pl.courier.clinic.common.exception;

public class BusinessException extends Exception {

    public BusinessException(String message) {
        super(message);
    }

}

