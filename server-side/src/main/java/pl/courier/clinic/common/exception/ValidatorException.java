package pl.courier.clinic.common.exception;

public class ValidatorException extends Exception {

    public ValidatorException(String message) {
        super(message);
    }
}
