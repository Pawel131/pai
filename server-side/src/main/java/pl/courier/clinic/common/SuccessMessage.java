package pl.courier.clinic.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SuccessMessage {
    private String description;
}
