package pl.courier.clinic.common.service;


import pl.courier.clinic.common.exception.BusinessException;

public class ParseTime {

    public static TimeInComponents parseFromString(String time) throws BusinessException {
        String[] timeInArray = time.split(":");
        if (timeInArray.length != 2) {
            throw new BusinessException("Unexpected Format of time");
        }
        return TimeInComponents.builder()
                .hours(Integer.valueOf(timeInArray[0]))
                .minutes(Integer.valueOf(timeInArray[1]))
                .build();
    }
}
