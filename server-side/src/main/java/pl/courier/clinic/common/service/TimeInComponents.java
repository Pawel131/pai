package pl.courier.clinic.common.service;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TimeInComponents {

    private long hours;
    private long minutes;
    private long seconds;
}
