package pl.courier.clinic.common.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AbstractDto implements Serializable {

    protected Long id;

}
