package pl.courier.clinic.common.service;

public class TimeUtils {

    public static TimeInComponents convertTime(long timeInSeconds) {
        TimeInComponents timeInComponents = new TimeInComponents();

        timeInComponents.setHours(timeInSeconds / 3600);
        timeInComponents.setMinutes((timeInSeconds % 3600) / 60);
        timeInComponents.setSeconds(timeInSeconds % 60);

        return timeInComponents;
    }
}
