package pl.courier.clinic.dental.service.business.logic;


import pl.courier.clinic.dental.model.Patient;

public interface PatientService {

    void save(Long personId);

    Patient findPatientByEmail(String email);

}
