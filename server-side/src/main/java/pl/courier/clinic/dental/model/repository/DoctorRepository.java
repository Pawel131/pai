package pl.courier.clinic.dental.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.courier.clinic.dental.model.Doctor;

@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Long> {

    @Query("select (count(p.email) > 0)"
            + "from Doctor d "
            + "join d.person  p "
            + "WHERE p.email like :email")
    boolean checkIsDoctor(@Param("email") String email);

}
