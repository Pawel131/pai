package pl.courier.clinic.dental.service.business.logic;

import pl.courier.clinic.dental.service.dto.WorkingDayPeriodsDto;
import pl.courier.clinic.dental.service.dto.WorkingDayWithPlannedVisitDto;
import pl.courier.clinic.dental.service.dto.WorkingPeriodDto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface WorkingDayPeriodService {

    void save(WorkingDayPeriodsDto dto);


    List<LocalTime> findWorkingPeriodsAndAssemblyToWorkingHours(LocalDate date);

    List<WorkingPeriodDto> getWorkingPeriodDto(LocalDate date);
}
