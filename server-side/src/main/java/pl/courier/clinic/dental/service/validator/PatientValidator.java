package pl.courier.clinic.dental.service.validator;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.courier.clinic.dental.model.repository.PatientRepository;
import pl.courier.clinic.common.exception.ValidatorException;

@Component
@AllArgsConstructor
public class PatientValidator {
    private final PatientRepository patientRepository;

    public boolean checkIsPatient(String email) throws ValidatorException {
        return patientRepository.checkIsPatient(email);
    }
}
