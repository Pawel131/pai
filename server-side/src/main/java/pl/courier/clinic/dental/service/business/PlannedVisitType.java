package pl.courier.clinic.dental.service.business;

public enum PlannedVisitType {
    NORMAL(30), REPAIR(45), WHITE(60);

    int minutes;

    PlannedVisitType(int minutes) {
        this.minutes = minutes;
    }

}
