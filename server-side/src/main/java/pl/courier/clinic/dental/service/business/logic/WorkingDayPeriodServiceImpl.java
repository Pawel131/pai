package pl.courier.clinic.dental.service.business.logic;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.courier.clinic.dental.model.WorkingDayPeriod;
import pl.courier.clinic.dental.model.WorkingPeriod;
import pl.courier.clinic.dental.model.repository.WorkingPeriodDayRepository;
import pl.courier.clinic.dental.service.assembler.WorkingDayPeriodsAssembler;
import pl.courier.clinic.dental.service.assembler.WorkingPeriodAssembler;
import pl.courier.clinic.dental.service.dto.WorkingDayDto;
import pl.courier.clinic.dental.service.dto.WorkingDayPeriodDto;
import pl.courier.clinic.dental.service.dto.WorkingDayPeriodsDto;
import pl.courier.clinic.dental.service.dto.WorkingPeriodDto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class WorkingDayPeriodServiceImpl implements WorkingDayPeriodService {
    private final WorkingDayService dayService;
    private final WorkingPeriodDayRepository workingPeriodDayRepository;
    private final WorkingPeriodService periodService;

    private final long ONE_VISIT_IN_MINUTES = 30;
    private final long TIME_OVERLAP = 1L;

    @Override
    @Transactional
    public void save(WorkingDayPeriodsDto dto) {
        WorkingDayDto workingDayDto = new WorkingDayDto();
        workingDayDto.setDate(dto.getDayDate());
        workingDayDto.setDoctorId(1L);

        Long workingDayId = dayService.save(workingDayDto);

        List<WorkingPeriodDto> periods = dto.getPeriods();
        List<WorkingDayPeriodDto> dayPeriodsDto = new ArrayList<>();

        periods.forEach(period -> {
            Long periodId = periodService.save(period);
            dayPeriodsDto.add(new WorkingDayPeriodDto(periodId, workingDayId));
        });

        List<WorkingDayPeriod> dayPeriod = dayPeriodsDto.stream()
                .map(WorkingDayPeriodsAssembler::toPeriodEntity).collect(Collectors.toList());
        dayPeriod.forEach(workingPeriodDayRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<LocalTime> findWorkingPeriodsAndAssemblyToWorkingHours(LocalDate date) {

        List<WorkingPeriod> workingPeriodInDate = workingPeriodDayRepository.findWorkingPeriodInDate(date);
        List<LocalTime> workingHours = new ArrayList<>();

        for (WorkingPeriod wp : workingPeriodInDate) {
            LocalTime startTime = wp.getStartTime();
            LocalTime finishTime = wp.getFinishTime().minusMinutes(ONE_VISIT_IN_MINUTES - TIME_OVERLAP);
            LocalTime temp = startTime;
            while (temp.isBefore(finishTime)) {
                workingHours.add(temp);
                temp = temp.plusMinutes(ONE_VISIT_IN_MINUTES);
            }
        }

        return workingHours;
    }

    @Override
    @Transactional(readOnly = true)
    public List<WorkingPeriodDto> getWorkingPeriodDto(LocalDate date) {
        List<WorkingPeriod> workingPeriodInDate = workingPeriodDayRepository.findWorkingPeriodInDate(date)
                .stream()
                .sorted(Comparator.comparing(WorkingPeriod::getStartTime))
                .collect(Collectors.toList());
        return convertPeriods(workingPeriodInDate);
    }

    private List<WorkingPeriodDto> convertPeriods(List<WorkingPeriod> workingPeriodInDate) {
        return workingPeriodInDate.stream().map(WorkingPeriodAssembler::toDto).collect(Collectors.toList());
    }
}
