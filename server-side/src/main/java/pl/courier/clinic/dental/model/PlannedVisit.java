package pl.courier.clinic.clinic.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.courier.clinic.dental.model.Patient;
import pl.courier.clinic.dental.model.PlannedVisitType;
import pl.courier.clinic.dental.model.WorkingDay;
import pl.courier.clinic.common.model.AbstractEntity;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(schema = "clinic", name = "planned_visits")
@Getter
@NoArgsConstructor
@Setter
public class PlannedVisit extends AbstractEntity {

    private static final long serialVersionUID = 6843756755854412053L;

    @ManyToOne
    @JoinColumn(name = "WORKING_DAY_ID")
    private WorkingDay workingDay;

    @ManyToOne
    @JoinColumn(name = "PATIENT_ID")
    private Patient patient;

    @ManyToOne
    @JoinColumn(name = "PLANNED_VISIT_TYPE_ID")
    private PlannedVisitType type;

    @Column(name = "START_TIME")
    private LocalTime startTime;

    public boolean isLongerThanHour() {
        return Integer.valueOf(getSpendMinutes()).compareTo(30) > 0;
    }

    public String getSpendMinutes() {
        return type.getMinutesSpend();
    }
}
