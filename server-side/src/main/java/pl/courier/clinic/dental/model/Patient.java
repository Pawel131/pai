package pl.courier.clinic.dental.model;


import lombok.Getter;
import lombok.Setter;
import pl.courier.clinic.common.model.AbstractEntity;
import pl.courier.clinic.customer.model.Person;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(schema = "clinic", name = "patients")
public class Patient extends AbstractEntity {
    private static final long serialVersionUID = -6739637423209134226L;

    @OneToOne
    private Person person;

    public String getPersonFullName() {
        return person.getFullName();
    }
}
