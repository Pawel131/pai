package pl.courier.clinic.dental.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.courier.clinic.clinic.model.PlannedVisit;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PlannedVisitRepository extends CrudRepository<PlannedVisit, Long> {

    @Query("select pv " +
            "from PlannedVisit pv " +
            "join pv.workingDay wd " +
            "where wd.date =:date")
    List<PlannedVisit> findAllInDay(@Param("date") LocalDate date);

    @Query("select pv " +
            "from PlannedVisit pv " +
            "join pv.workingDay wd " +
            "join pv.patient pt " +
            "join pt.person p " +
            "where wd.date > :date " +
            "and p.email = :email ")
    List<PlannedVisit> findAllFutureVisits(@Param("date") LocalDate date, @Param("email") String email);
}

