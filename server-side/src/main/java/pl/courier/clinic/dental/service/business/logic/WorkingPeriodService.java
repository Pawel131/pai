package pl.courier.clinic.dental.service.business.logic;

import pl.courier.clinic.dental.service.dto.WorkingPeriodDto;

public interface WorkingPeriodService {

    Long save(WorkingPeriodDto dto);


}
