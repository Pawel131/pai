package pl.courier.clinic.dental.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.courier.clinic.dental.model.Patient;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {

    @Query("select (count(p.email) > 0)"
            + "from Patient pt "
            + "join pt.person  p "
            + "WHERE p.email like :email")
    boolean checkIsPatient(@Param("email") String email);

    @Query("select pt "
            + "from Patient pt "
            + "join pt.person  p "
            + "WHERE p.email like :email")
    Patient findPatientByEmail(@Param("email") String email);

}
