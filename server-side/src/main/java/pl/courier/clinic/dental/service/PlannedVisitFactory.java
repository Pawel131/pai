package pl.courier.clinic.dental.service;

import pl.courier.clinic.clinic.model.PlannedVisit;
import pl.courier.clinic.dental.model.PlannedVisitType;

import java.time.LocalTime;

public class PlannedVisitFactory {

    public static PlannedVisit create(LocalTime startTime, pl.courier.clinic.dental.service.business.PlannedVisitType visitType) {
        PlannedVisit plannedVisit = new PlannedVisit();
        PlannedVisitType plannedVisitType = new PlannedVisitType();
        switch (visitType) {
            case NORMAL:
                plannedVisitType.setMinutesSpend("30");
                break;
            case WHITE:
                plannedVisitType.setMinutesSpend("60");
                break;
            case REPAIR:
                plannedVisitType.setMinutesSpend("45");
                break;
        }

        plannedVisit.setStartTime(startTime);
        plannedVisit.setType(plannedVisitType);
        return plannedVisit;

    }
}
