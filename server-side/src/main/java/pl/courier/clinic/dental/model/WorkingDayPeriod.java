package pl.courier.clinic.dental.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "clinic", name = "working_day_periods")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorkingDayPeriod {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "working_period_id",
            insertable = false, updatable = false)
    private WorkingPeriod workingPeriod;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "working_day_id",
            insertable = false, updatable = false)
    private WorkingDay workingDay;
    @EmbeddedId
    private Id id = new Id();

    public WorkingDayPeriod(WorkingDay workingDay, WorkingPeriod workingPeriod) {
        this.workingDay = workingDay;
        this.workingPeriod = workingPeriod;
        this.id.workingDayId = workingDay.getId();
        this.id.workingPeriodId = workingPeriod.getId();
    }

    @Embeddable
    @AllArgsConstructor
    @EqualsAndHashCode
    @NoArgsConstructor
    private static class Id implements Serializable {
        private static final long serialVersionUID = 2792246746095487077L;
        @Column(name = "working_day_id")
        private Long workingDayId;
        @Column(name = "working_period_id")
        private Long workingPeriodId;
    }
}
