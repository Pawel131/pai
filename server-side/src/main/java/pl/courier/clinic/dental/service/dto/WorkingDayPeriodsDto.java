package pl.courier.clinic.dental.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class WorkingDayPeriodsDto {
    private LocalDate dayDate;
    private List<WorkingPeriodDto> periods;
}
