package pl.courier.clinic.dental.model;

import lombok.Getter;
import lombok.Setter;
import pl.courier.clinic.common.model.AbstractEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(schema = "clinic", name = "working_days")
@Getter
@Setter
public class WorkingDay extends AbstractEntity {

    private static final long serialVersionUID = 8010042662123272445L;

    private LocalDate date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DOCTOR_ID")
    private Doctor doctor;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "workingDay")
    private Set<WorkingDayPeriod> workingDayPeriods;

}
