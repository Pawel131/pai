package pl.courier.clinic.dental.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.courier.clinic.dental.model.WorkingDayPeriod;
import pl.courier.clinic.dental.model.WorkingPeriod;

import java.time.LocalDate;
import java.util.List;

public interface WorkingPeriodDayRepository extends CrudRepository<WorkingDayPeriod, Long> {

    @Query("select  wp " +
            "from WorkingDayPeriod wdp " +
            "join wdp.workingPeriod wp " +
            "join wdp.workingDay wd " +
            "where wd.date = :date")
    List<WorkingPeriod> findWorkingPeriodInDate(@Param("date") LocalDate date);
}
