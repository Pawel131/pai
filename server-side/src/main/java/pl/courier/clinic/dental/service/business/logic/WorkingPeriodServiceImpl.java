package pl.courier.clinic.dental.service.business.logic;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.courier.clinic.dental.model.WorkingPeriod;
import pl.courier.clinic.dental.model.repository.WorkingPeriodRepository;
import pl.courier.clinic.dental.service.assembler.WorkingDayPeriodsAssembler;
import pl.courier.clinic.dental.service.dto.WorkingPeriodDto;

@Service
@AllArgsConstructor
public class WorkingPeriodServiceImpl implements WorkingPeriodService {
    private final WorkingPeriodRepository periodRepository;

    @Override
    @Transactional
    public Long save(WorkingPeriodDto dto) {
        WorkingPeriod save = periodRepository.save(WorkingDayPeriodsAssembler.toEntity(dto));
        return save.getId();

    }
}
