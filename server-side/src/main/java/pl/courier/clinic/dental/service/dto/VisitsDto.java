package pl.courier.clinic.dental.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class VisitsDto {
    private List<VisitDto> visits;

}
