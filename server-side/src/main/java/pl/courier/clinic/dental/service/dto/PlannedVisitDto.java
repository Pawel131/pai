package pl.courier.clinic.dental.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PlannedVisitDto {
    private LocalDate date;
    private String startHour;


}
