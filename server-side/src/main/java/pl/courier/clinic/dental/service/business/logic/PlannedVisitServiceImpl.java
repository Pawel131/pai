package pl.courier.clinic.dental.service.business.logic;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.courier.clinic.clinic.model.PlannedVisit;
import pl.courier.clinic.common.exception.BusinessException;
import pl.courier.clinic.dental.model.Patient;
import pl.courier.clinic.dental.model.WorkingDay;
import pl.courier.clinic.dental.model.repository.PlannedVisitRepository;
import pl.courier.clinic.dental.service.assembler.PlannedVisitAssembler;
import pl.courier.clinic.dental.service.dto.PlannedVisitDto;
import pl.courier.clinic.dental.service.dto.PlannedVisitListDto;
import pl.courier.clinic.dental.service.dto.VisitDto;
import pl.courier.clinic.dental.service.dto.WorkingDayWithPlannedVisitDto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PlannedVisitServiceImpl implements PlannedVisitService {

    private final WorkingDayPeriodService workingDayPeriodService;
    private final PlannedVisitRepository plannedVisitRepository;
    private final PatientService patientService;
    private final WorkingDayService workingDayService;

    @Override
    @Transactional(readOnly = true)
    public List<String> getFreeHoursInDay(LocalDate day) {
        List<LocalTime> workingHour = workingDayPeriodService.findWorkingPeriodsAndAssemblyToWorkingHours(day);
        List<LocalTime> visits = getPlannedVisitHours(day);

        return workingHour.stream()
                .filter(workHour -> !visits.contains(workHour))
                .map(freeHour -> freeHour.format(DateTimeFormatter.ofPattern("H:mm")))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<LocalTime> getPlannedVisitHours(LocalDate day) {
        return plannedVisitRepository.findAllInDay(day).stream()
                .map(this::getListOfHoursByVisitType)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

    }


    @Override
    @Transactional(readOnly = true)
    public List<PlannedVisitListDto> getPlannedVisits(LocalDate day) {
        return plannedVisitRepository.findAllInDay(day)
                .stream()
                .map(PlannedVisitAssembler::toPlannedVisitListDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public WorkingDayWithPlannedVisitDto findAllByDate(LocalDate date) {

        return WorkingDayWithPlannedVisitDto
                .builder()
                .date(date)
                .visits(getPlannedVisits(date))
                .periods(workingDayPeriodService.getWorkingPeriodDto(date))
                .build();
    }


    @Override
    @Transactional
    public void save(PlannedVisitDto dto, String userEmail) throws BusinessException {
        Patient patient = patientService.findPatientByEmail(userEmail);
        WorkingDay workingDay = workingDayService.findByDate(dto.getDate());
        PlannedVisit plannedVisit = PlannedVisitAssembler.toEntityWithDefaultVisitType(dto, patient, workingDay);

        plannedVisitRepository.save(plannedVisit);
    }

    @Override
    @Transactional(readOnly = true)
    public List<VisitDto> findAllFutureVisits(String email) {
        return plannedVisitRepository.findAllFutureVisits(LocalDate.now(), email)
                .stream()
                .map(PlannedVisitAssembler::visitDto)
                .collect(Collectors.toList());
    }

    private List<LocalTime> getListOfHoursByVisitType(PlannedVisit visit) {
        if (visit.isLongerThanHour()) {
            ArrayList<LocalTime> hours = new ArrayList<>();
            hours.add(visit.getStartTime());
            hours.add(visit.getStartTime().plusMinutes(30));
            return hours;
        } else {
            return Collections.singletonList(visit.getStartTime());
        }
    }

}
