package pl.courier.clinic.dental.service.validator;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.courier.clinic.dental.model.repository.DoctorRepository;
import pl.courier.clinic.common.exception.ValidatorException;

@Component
@AllArgsConstructor
public class DoctorValidator {

    private final DoctorRepository doctorRepository;

    public boolean checkIsDoctor(String email) throws ValidatorException {
        return doctorRepository.checkIsDoctor(email);
    }

}
