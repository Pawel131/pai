package pl.courier.clinic.dental.model;


import lombok.Getter;
import lombok.Setter;
import pl.courier.clinic.common.model.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "dict", name = "planned_visit_types")
@Getter
@Setter
public class PlannedVisitType extends AbstractEntity {

    private static final long serialVersionUID = 7767042513271336683L;
    private String minutesSpend;

    private String name;
}
