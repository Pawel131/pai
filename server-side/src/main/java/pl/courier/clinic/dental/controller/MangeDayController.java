package pl.courier.clinic.dental.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.courier.clinic.common.SuccessMessage;
import pl.courier.clinic.dental.model.WorkingDay;
import pl.courier.clinic.dental.service.business.logic.WorkingDayPeriodService;
import pl.courier.clinic.dental.service.business.logic.WorkingDayService;
import pl.courier.clinic.dental.service.dto.WorkingDayPeriodsDto;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin
public class MangeDayController {

    private final WorkingDayPeriodService workingDayPeriodService;
    private final WorkingDayService dayService;

    @PostMapping("/modifyWorkingDay")
    public SuccessMessage modifyWorkingDay(@RequestBody WorkingDayPeriodsDto dto) {
        workingDayPeriodService.save(dto);
        return new SuccessMessage("Working day successfully added.");
    }

    @GetMapping("/getPeriods")
    public List<WorkingDay> getPeriods() {
        return dayService.findAll();
    }

}
