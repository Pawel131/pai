package pl.courier.clinic.dental.service.dto;

import lombok.Getter;
import lombok.Setter;
import pl.courier.clinic.common.service.dto.AbstractDto;

import java.time.LocalDate;

@Getter
@Setter
public class WorkingDayDto extends AbstractDto {
    private static final long serialVersionUID = -6782648786246675575L;

    private LocalDate date;
    private Long doctorId;
}
