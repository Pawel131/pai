package pl.courier.clinic.dental.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.courier.clinic.dental.model.WorkingPeriod;

@Repository
public interface WorkingPeriodRepository extends CrudRepository<WorkingPeriod, Long> {
}
