package pl.courier.clinic.dental.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.courier.clinic.dental.service.business.logic.PlannedVisitService;
import pl.courier.clinic.dental.service.dto.WorkingDateDto;
import pl.courier.clinic.dental.service.dto.WorkingDayWithPlannedVisitDto;

@RestController
@AllArgsConstructor
@CrossOrigin
public class DoctorController {

    private final PlannedVisitService plannedVisitService;


    @PostMapping("/workingDayVisits")
    public WorkingDayWithPlannedVisitDto workingDayVisits(@RequestBody WorkingDateDto date) {
        return plannedVisitService.findAllByDate(date.getDate());
    }
}
