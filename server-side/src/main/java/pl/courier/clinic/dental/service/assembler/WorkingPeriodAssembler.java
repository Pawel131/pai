package pl.courier.clinic.dental.service.assembler;

import pl.courier.clinic.dental.model.WorkingPeriod;
import pl.courier.clinic.dental.service.dto.WorkingPeriodDto;

import java.time.format.DateTimeFormatter;

public class WorkingPeriodAssembler {

    public static WorkingPeriodDto toDto(WorkingPeriod entity) {
        WorkingPeriodDto dto = new WorkingPeriodDto();
        dto.setStart(entity.getStartTime().format(DateTimeFormatter.ofPattern("H:mm")));
        dto.setFinish(entity.getFinishTime().format(DateTimeFormatter.ofPattern("H:mm")));
        return dto;
    }
}
