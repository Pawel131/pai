package pl.courier.clinic.dental.model;

import lombok.Getter;
import lombok.Setter;
import pl.courier.clinic.common.model.AbstractEntity;
import pl.courier.clinic.customer.model.Person;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(schema = "clinic", name = "doctors")
public class Doctor extends AbstractEntity {
    private static final long serialVersionUID = 3025329973647964307L;

    @OneToOne
    private Person person;

}
