package pl.courier.clinic.dental.service.business.logic;

import pl.courier.clinic.dental.model.WorkingDay;
import pl.courier.clinic.dental.service.dto.WorkingDayDto;

import java.time.LocalDate;
import java.util.List;

public interface WorkingDayService {

    Long save(WorkingDayDto dto);

    List<WorkingDay> findAll();

    WorkingDay findByDate(LocalDate date);
}
