package pl.courier.clinic.dental.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class WorkingHoursDto {

    private List<String> hours;

    public WorkingHoursDto() {
        hours = new ArrayList<>();
    }

    public void addHour(String hour) {
        hours.add(hour);
    }
}
