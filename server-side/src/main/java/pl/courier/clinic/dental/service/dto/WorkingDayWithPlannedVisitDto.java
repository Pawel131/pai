package pl.courier.clinic.dental.service.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkingDayWithPlannedVisitDto {

    private LocalDate date;
    private List<WorkingPeriodDto> periods;
    private List<PlannedVisitListDto> visits;
}
