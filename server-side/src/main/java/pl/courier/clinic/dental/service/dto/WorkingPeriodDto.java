package pl.courier.clinic.dental.service.dto;

import lombok.Getter;
import lombok.Setter;
import pl.courier.clinic.common.service.dto.AbstractDto;

@Getter
@Setter
public class WorkingPeriodDto extends AbstractDto {
    private static final long serialVersionUID = -6784705010181533155L;

    private String start;
    private String finish;
}
