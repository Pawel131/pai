package pl.courier.clinic.dental.service.business.logic;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.courier.clinic.dental.model.WorkingDay;
import pl.courier.clinic.dental.model.repository.WorkingDayRepository;
import pl.courier.clinic.dental.service.assembler.WorkingDayPeriodsAssembler;
import pl.courier.clinic.dental.service.dto.WorkingDayDto;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
public class WorkingDayServiceImpl implements WorkingDayService {
    private final WorkingDayRepository workingDayRepository;

    @Override
    @Transactional
    public Long save(WorkingDayDto dto) {
        WorkingDay save = workingDayRepository.save(WorkingDayPeriodsAssembler.toEntity(dto));
        return save.getId();
    }

    @Override
    @Transactional(readOnly = true)
    public List<WorkingDay> findAll() {
        List<WorkingDay> all = workingDayRepository.findAll();
        return all;
    }

    @Override
    @Transactional(readOnly = true)
    public WorkingDay findByDate(LocalDate date) {
        return workingDayRepository.findByDate(date);
    }
}
