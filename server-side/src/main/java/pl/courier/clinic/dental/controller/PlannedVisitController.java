package pl.courier.clinic.dental.controller;

import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import pl.courier.clinic.common.SuccessMessage;
import pl.courier.clinic.dental.service.business.logic.PlannedVisitService;
import pl.courier.clinic.dental.service.dto.PlannedVisitDto;
import pl.courier.clinic.dental.service.dto.VisitDto;
import pl.courier.clinic.dental.service.dto.WorkingDateDto;
import pl.courier.clinic.common.exception.BusinessException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin
public class PlannedVisitController {
    private final PlannedVisitService plannedVisitService;

    @PostMapping("/getVisitInDays")
    public List<LocalTime> getVisitInDays(@RequestBody LocalDate date) {
        return plannedVisitService.getPlannedVisitHours(date);
    }

    @GetMapping("/visits")
    public List<VisitDto> visits(@AuthenticationPrincipal UserDetails userDetails) {
        return plannedVisitService.findAllFutureVisits(userDetails.getUsername());
    }

    @PostMapping("/getFreeHoursInDate")
    public List<String> getFreeHoursInDate(@RequestBody WorkingDateDto date) {
        return plannedVisitService.getFreeHoursInDay(date.getDate());
    }

    @PostMapping("/confirmVisit")
    public SuccessMessage confirmVisit(@AuthenticationPrincipal UserDetails user, @RequestBody PlannedVisitDto dto) throws BusinessException {
        plannedVisitService.save(dto, user.getUsername());
        return new SuccessMessage("Visit successfully confirm. We will contact with you.");
    }

}
