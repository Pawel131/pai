package pl.courier.clinic.dental.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.courier.clinic.dental.model.WorkingDay;

import java.time.LocalDate;

@Repository
public interface WorkingDayRepository extends JpaRepository<WorkingDay, Long> {
    @Query("select wd " +
            "from WorkingDay wd " +
            "join wd.doctor d " +
            "where wd.date =:date " +
            "and d.id = 1")
    WorkingDay findByDate(@Param("date") LocalDate date);
}
