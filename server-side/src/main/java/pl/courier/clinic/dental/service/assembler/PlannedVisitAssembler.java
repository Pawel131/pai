package pl.courier.clinic.dental.service.assembler;

import pl.courier.clinic.clinic.model.PlannedVisit;
import pl.courier.clinic.common.exception.BusinessException;
import pl.courier.clinic.common.service.ParseTime;
import pl.courier.clinic.common.service.TimeInComponents;
import pl.courier.clinic.dental.model.Patient;
import pl.courier.clinic.dental.model.PlannedVisitType;
import pl.courier.clinic.dental.model.WorkingDay;
import pl.courier.clinic.dental.service.dto.PlannedVisitDto;
import pl.courier.clinic.dental.service.dto.PlannedVisitListDto;
import pl.courier.clinic.dental.service.dto.VisitDto;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class PlannedVisitAssembler {

    public static PlannedVisit toEntityWithDefaultVisitType(PlannedVisitDto dto, Patient patient, WorkingDay wd) throws BusinessException {
        PlannedVisit plannedVisit = new PlannedVisit();
        PlannedVisitType type = new PlannedVisitType();
        // it is set temporary
        type.setId(1L);
        plannedVisit.setType(type);
        plannedVisit.setStartTime(getLocalTime(dto.getStartHour()));

        plannedVisit.setPatient(patient);
        plannedVisit.setWorkingDay(wd);

        return plannedVisit;
    }

    private static LocalTime getLocalTime(String time) throws BusinessException {
        TimeInComponents timeInComponents = ParseTime.parseFromString(time);
        return LocalTime.of((int) timeInComponents.getHours(), (int) timeInComponents.getMinutes());
    }

    public static VisitDto visitDto(PlannedVisit entity) {
        VisitDto visitDto = new VisitDto();
        visitDto.setTime(entity.getStartTime().format(DateTimeFormatter.ofPattern("H:mm")));
        visitDto.setDate(entity.getWorkingDay().getDate().format(DateTimeFormatter.ofPattern("YYYY/MM/DD")));

        return visitDto;
    }

    public static PlannedVisitListDto toPlannedVisitListDto(PlannedVisit entity) {
        PlannedVisitListDto plannedVisitListDto = new PlannedVisitListDto();
        plannedVisitListDto.setStartHour(entity.getStartTime().format(DateTimeFormatter.ofPattern("H:mm")));
        Patient patient = entity.getPatient();
        plannedVisitListDto.setPatientName(patient.getPersonFullName());

        return plannedVisitListDto;
    }

}
