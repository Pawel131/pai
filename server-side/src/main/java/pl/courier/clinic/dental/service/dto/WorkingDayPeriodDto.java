package pl.courier.clinic.dental.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class WorkingDayPeriodDto {
    private Long periodId;
    private Long workingDayId;
}
