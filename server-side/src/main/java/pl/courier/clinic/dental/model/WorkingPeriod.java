package pl.courier.clinic.dental.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.courier.clinic.common.model.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Set;

@Entity
@Table(schema = "dict", name = "working_periods")
@Getter
@Setter
@NoArgsConstructor
public class WorkingPeriod extends AbstractEntity {
    private static final long serialVersionUID = -4333648601773272727L;

    private LocalTime startTime;
    private LocalTime finishTime;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "workingPeriod")
    private Set<WorkingDayPeriod> workingDayPeriods;


    public WorkingPeriod(LocalTime startTime, LocalTime finishTime) {
        this.startTime = startTime;
        this.finishTime = finishTime;
    }

    public long getWorkingSeconds() {
        return Duration.between(startTime, finishTime).getSeconds();
    }
}
