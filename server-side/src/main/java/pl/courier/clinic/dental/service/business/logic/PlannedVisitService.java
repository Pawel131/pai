package pl.courier.clinic.dental.service.business.logic;

import pl.courier.clinic.common.exception.BusinessException;
import pl.courier.clinic.dental.service.dto.PlannedVisitDto;
import pl.courier.clinic.dental.service.dto.PlannedVisitListDto;
import pl.courier.clinic.dental.service.dto.VisitDto;
import pl.courier.clinic.dental.service.dto.WorkingDayWithPlannedVisitDto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface PlannedVisitService {

    List<String> getFreeHoursInDay(LocalDate day);

    List<LocalTime> getPlannedVisitHours(LocalDate day);

    List<PlannedVisitListDto> getPlannedVisits(LocalDate day);

    WorkingDayWithPlannedVisitDto findAllByDate(LocalDate date);

    void save(PlannedVisitDto dto, String userEmail) throws BusinessException;

    List<VisitDto> findAllFutureVisits(String userEmail);
}
