package pl.courier.clinic.dental.service.assembler;

import pl.courier.clinic.dental.model.Doctor;
import pl.courier.clinic.dental.model.WorkingDay;
import pl.courier.clinic.dental.model.WorkingDayPeriod;
import pl.courier.clinic.dental.model.WorkingPeriod;
import pl.courier.clinic.dental.service.dto.WorkingDayDto;
import pl.courier.clinic.dental.service.dto.WorkingDayPeriodDto;
import pl.courier.clinic.dental.service.dto.WorkingPeriodDto;

import java.time.LocalTime;

public class WorkingDayPeriodsAssembler {

    public static WorkingDay toEntity(WorkingDayDto dto) {
        WorkingDay entity = new WorkingDay();
        entity.setId(dto.getId());
        entity.setDate(dto.getDate());
        Doctor doctor = new Doctor();
        doctor.setId(dto.getDoctorId());
        entity.setDoctor(doctor);
        return entity;
    }

    public static WorkingPeriod toEntity(WorkingPeriodDto dto) {
        WorkingPeriod entity = new WorkingPeriod();
        entity.setId(dto.getId());
        String[] splittedFinishTime = dto.getFinish().split(":");
        String[] splittedStartTime = dto.getStart().split(":");
        entity.setFinishTime(LocalTime.of(Integer.valueOf(splittedFinishTime[0])
                , Integer.valueOf(splittedFinishTime[1])));
        entity.setStartTime(LocalTime.of(Integer.valueOf(splittedStartTime[0])
                , Integer.valueOf(splittedStartTime[1])));

        return entity;
    }

    public static WorkingDayPeriod toPeriodEntity(WorkingDayPeriodDto dto) {

        WorkingDay workingDay = new WorkingDay();
        WorkingPeriod workingPeriod = new WorkingPeriod();
        workingDay.setId(dto.getWorkingDayId());
        workingPeriod.setId(dto.getPeriodId());

        return new WorkingDayPeriod(workingDay, workingPeriod);
    }
}
