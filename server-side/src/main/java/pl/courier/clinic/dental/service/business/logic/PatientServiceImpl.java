package pl.courier.clinic.dental.service.business.logic;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.courier.clinic.dental.model.Patient;
import pl.courier.clinic.dental.model.repository.PatientRepository;
import pl.courier.clinic.customer.model.Person;

@AllArgsConstructor
@Service
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    @Override
    @Transactional
    public void save(Long personId) {
        Patient patient = new Patient();
        Person person = new Person();
        person.setId(personId);
        patient.setPerson(person);

        patientRepository.save(patient);
    }

    @Override
    @Transactional(readOnly = true)
    public Patient findPatientByEmail(String email) {
        return patientRepository.findPatientByEmail(email);
    }
}
